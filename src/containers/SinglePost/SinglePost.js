import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label} from "reactstrap";
import ProductThumbnail from "../../components/ProductThumbnail/ProductThumbnail";
import {createComment, fetchComments, getSinglePost} from "../../store/actions/postsActions";



class SinglePost extends Component {

    state={
        comment: '',
    };

    componentDidMount(){
        this.props.fetchPost(this.props.match.params.id);
        this.props.fetchComments(this.props.match.params.id);
    }


    submitFormHandler = event => {
        event.preventDefault();
        this.props.commentCreated({...this.state, postId: this.props.match.params.id}, this.props.match.params.id)
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        if(!this.props.post) return null;
        return (
            <Fragment>
                <div>
                    <h2 style={{margin: "0 auto"}}>{this.props.post.title}</h2>
                    <p>{this.props.post.dateTime}</p>&nbsp;
                    <ProductThumbnail image={this.props.post.image}/>
                    <span>{this.props.post.description}</span>&nbsp;
                </div>

                &nbsp;<h2>Comments</h2>
                {this.props.comments.length > 0 ? this.props.comments.map(comment =>(
                    <Card key = {comment.id}>
                        <CardBody>
                            <span  style={{marginLeft: "20px"}}><strong style={{textTransform: "uppercase"}}>{comment.username.username}</strong>&nbsp; wrote:</span>&nbsp;
                            <span>{comment.comment}</span>&nbsp;
                        </CardBody>
                    </Card>
                    ))


                : <p>Sign in to  view a comments </p>}&nbsp;

                {this.props.user ? <div>
                    &nbsp;<h2>Add Comment</h2>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup row>
                            <Label sm={2} for="comment">type a comment</Label>
                            <Col sm={10}>
                                <Input
                                    type="textarea" required
                                    name="comment" id="comment"
                                    placeholder="Enter your text"
                                    value={this.state.comment}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{offset:2, size: 10}}>
                                <Button type="submit" color="primary">Add</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div> : null}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    post: state.posts.post,
    comments: state.posts.comments,
    user: state.users.user
});

const mapDispatchToProps = dispatch =>({
    fetchPost: postId => dispatch(getSinglePost(postId)),
    fetchComments: comId =>dispatch(fetchComments(comId)),
    commentCreated: (comData, id) => dispatch(createComment(comData, id)),
});

export default connect(mapStateToProps,mapDispatchToProps)(SinglePost);