import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import PostListItem from "../../components/ListItems/PostListItem";
import {fetchPosts} from "../../store/actions/postsActions";



class Posts extends Component {
    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        return (
            <Fragment>
                <h2>
                    Posts
                </h2>
                {this.props.posts.length > 0 ? this.props.posts.map(post => {
                    console.log(post.username.username);
                    return <PostListItem
                        key={post._id}
                        _id={post._id}
                        title={post.title}
                        user={post.username.username}
                        image={post.image}
                        dateTime={post.dateTime}
                    />
                }) : <p>no posts</p>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts,
});

const mapDispatchToProps = dispatch => ({
    fetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);


