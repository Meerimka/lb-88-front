import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const FETCH_POST_SUCCESS ='CREATE_POST_SUCCESS';
export const FETCH_COMMENTS_SUCCESS ='FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENT_SUCCESS ='FETCH_COMMENT_SUCCESS';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostSuccess = post =>({type: FETCH_POST_SUCCESS, post});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const fetchCommentsSuccess = comments =>({type: FETCH_COMMENTS_SUCCESS, comments});
export const createCommentSuccess = (comment) =>({type: FETCH_COMMENT_SUCCESS, comment});

export const fetchPosts = () => {
    return dispatch => {
        return axios.get('/posts').then(
            response => {
                console.log(response.data);
                dispatch(fetchPostsSuccess(response.data))
            }
        );
    };
};

export const createPost = postsData => {
    return (dispatch, getState) => {

        const user = getState().users.user;

        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/posts', postsData, {headers: {'Authorization': user.token}}).then(
                () => {
                    dispatch(createPostSuccess());
                    dispatch(push('/'))
                }

            );
        }

    };
};

export const getSinglePost = (postId) => {
    return dispatch => {
        return axios.get('/posts/' + postId).then(
            response => {
                console.log(response.data);
                return dispatch(fetchPostSuccess(response.data));
            },

        );
    }
};

export const fetchComments = postId =>{
    return (dispatch, getState) =>{
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/posts/' + postId));
        }else {
            return axios.get('/comments/' + postId,{headers: {'Authorization': user.token}} ).then(
                response => {
                    console.log(response.data);
                    return dispatch(fetchCommentsSuccess(response.data))
                }
            );
        }
    };
};

export const createComment = (comData, id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/comments', comData,{headers: {'Authorization': user.token}} ).then(
                (response) => {
                    dispatch(fetchComments(id));
                }
            )
        }

    }
};