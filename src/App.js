import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";
import {Switch, Route, withRouter} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {logoutUser} from "./store/actions/usersActions";
import Posts from "./containers/Posts/Posts";
import NewPost from "./containers/Posts/NewPost";
import SinglePost from "./containers/SinglePost/SinglePost";




class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch >
                        <Route path="/" exact component={Posts}/>
                        <Route path="/posts/:id" exact component={SinglePost}/>
                        <Route path="/post/new" exact component={NewPost}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state =>({
    user: state.users.user
});

const mapDispatchToProps = dispatch =>({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
