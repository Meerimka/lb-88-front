import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {Link} from "react-router-dom";

const UserMenu = ({user,logout}) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            Hello, {user.username}!
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem>
                <Link to="/post/new">
                      Add new post
                </Link>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);



export default UserMenu;