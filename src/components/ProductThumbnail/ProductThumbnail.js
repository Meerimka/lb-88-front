import React from 'react';
import Messages from '../../assets/images/messages.png';
import {apiURL} from "../../constants";

const style = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};

const ProductThumbnail = props => {
    let image = Messages;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    return <img src={image} style={style} className="img-thumbnail" alt="Something here" />
};

export default ProductThumbnail;