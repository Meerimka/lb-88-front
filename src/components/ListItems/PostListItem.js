import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";
import {Link} from "react-router-dom";

const PostListItem = props => {
    return (
        <Fragment>
            <div >
                <Link to={'/posts/' + props._id}>
                    <Card>
                        <CardBody>
                            <ProductThumbnail image={props.image}/>
                            <h2>{props.title}</h2>
                            <p>
                                <span className="text-muted" >username : &nbsp;</span> {props.user}
                            </p>
                            <span className="text-muted">datetime : &nbsp;</span> {props.dateTime}
                        </CardBody>
                    </Card>
                </Link>

            </div>
        </Fragment>
    );
};

PostListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    dateTime: PropTypes.string,
    image: PropTypes.string
};
export default PostListItem;